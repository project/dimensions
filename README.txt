Dimensions
----------

Project Page:
http://drupal.org/project/dimensions

By Garrett Albright
http://drupal.org/user/191212
Email: albright (at) abweb [dot] us
AIM: AlbrightGuy - ICQ: 150670553 - IRC: Albright

Installation & Configuration
----------------------------

For full installation and configuration instructions, please see this page in the Drupal manual:
http://drupal.org/node/819832